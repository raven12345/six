#include<iostream>
#include<fstream>
#include"json\json.h"
#include<time.h>
using namespace std;
#pragma comment(lib, "lib_json.lib")
int main()
{
	srand(time(NULL));
	int a, b, i, j;
	std::string weeks[7] = { "Mon","Tue","Wed","Thu","Fri","Sat","Sun" };
	std::string tags[50] = { "stklv","sibhp","uikuj","dzeyg","fbume","mwcyl","xwzjc","pszzn","oarzv","sgspf","ynrax","inrfg","pwvss","phsxl","hwjtt","nhyng","rfhhl","xmuuk","ffscx","azmrn","uepos","ajymm","edbkm","hcbsx","nnrsf","iqxus","cvehi","iapwx","zyxab","musem","ghcls","zsnpk","dtzau","lrhdf","llkax","gfmmo","frctw","xivlc","lrguy","yyibw","vfnuq","dgmbs","fubgx","mwfil","qsnkv","fgdaa","edexr","lnkfd","racbw","leqbf" };
	Json::Value root;
	Json::Value next;
	Json::Reader reader;
	Json::FastWriter fwriter;
	Json::StyledWriter swriter;
	char department_name_a[6];
	char department_no_a[4];
	srand(time(NULL));
	cin >> a >> b;
	std::string name[100];
	for (i = 0;i < a;i++) {
		
		department_no_a[0] = (i / 100) % 10 + '0';
		department_no_a[1] = (i / 10) % 10 + '0';
		department_no_a[2] = i % 10 + '0';
		department_no_a[3] = '\0';
		string department_no_b = (&department_no_a[0]);
		department_name_a[0] = i%26 + 'a';
		department_name_a[1] = (rand()*2+1)%26 + 'a';
		department_name_a[2] = (rand()*3+2)%26 + 'a';
		department_name_a[3] = (rand()*4+3)%26 + 'a';
		department_name_a[4] = (rand()*5+4)%26 + 'a';
		department_name_a[5] = '\0';
		string department_name_b = (&department_name_a[0]);
		name[i] = "FZU_" + department_no_b;
		next["department_no"] = Json::Value("FZU_"+department_no_b);
		next["department_name"] = Json::Value("abc"+ department_name_b);
		next["member_limit"]=Json::Value(rand() % 16);
		int k[8] = {60};	//记录是否出现重复的tags;
		int e;
		for (j = rand() % 8;j >= 0;j--) {
			k[7 - j] = rand()%50;
			for (e = 7 - j;e < 7;e++) {
				if (k[7 - j] == k[e] && 7 - j != e) {
					e = 8;
					break;
				}
			}
			if (e == 8) {
				continue;
			}
			next["tags"].append(tags[k[7-j]]);
		}
		
		for (j = rand() % 3 + 1;j > 0;j--) {
			char timestart[3];
			int ran = rand() % 15 + 7;
			timestart[0] = ran / 10 + '0';
			timestart[1] = ran % 10 + '0';
			timestart[2] = '\0';
			std::string timest = (&timestart[0]);
			char timeend[3];
			int en = ran + (rand() % 2 + 1);
			timeend[0] = en / 10 + '0';
			timeend[1] = en % 10 + '0';
			timeend[2] = '\0';
			std::string timeen = (&timeend[0]);
			next["event_schedules"].append(weeks[(rand() % 2 + 1 + (5 - j)) % 7] + timest + ":00--" + timeen + ":00" );
		}
		for (j = 0;j < 8;j++) {
			k[j] = 80;
		}
		root["departments"].append(next);
		next.clear();
	}
	char student_no[6] = { '0' };
	char student_nam[6] = { '0' };
	for (i = 0;i < b;i++) {
		student_no[0] = i / 10000 + '0';
		student_no[1] = (i / 1000) % 10 + '0';
		student_no[2] = (i / 100) % 10 + '0';
		student_no[3] = (i / 10) % 10 + '0';
		student_no[4] = i % 10 + '0';
		std::string student_n = (&student_no[0]);
		next["student_no"] = student_n;
		student_nam[0] = rand() % 26 + 'A';
		student_nam[1] = rand() % 26 + 'A';
		student_nam[2] = rand() % 26 + 'A';
		student_nam[3] = rand() % 26 + 'A';
		student_nam[4] = rand() % 26 + 'A';
		student_nam[5] = '\0';
		std::string student_name = (&student_nam[0]);
		next["student_name"] = student_name;
		int k[8] = { 60 };	//记录是否出现重复的tags;
		int e;
		for (j = rand() % 8;j >= 0;j--) {
			k[7 - j] = rand() % 50;
			for (e = 7 - j;e >=0;e--) {
				if (k[7 - j] == k[e] && 7 - j != e) {
					e = 8;
					break;
				}
			}
			if (e == 8) {
				continue;
			}
			next["tags"].append(tags[k[7 - j]]);
		}
		for (j = 0;j < 8;j++) {
			k[j] = 200;
		}
		for (j = rand() % 5 + 3;j > 0;j--) {
			char timestart[3];
			int ran = rand() % 15 + 7;
			timestart[0] = ran / 10 + '0';
			timestart[1] = ran % 10 + '0';
			timestart[2] = '\0';
			std::string timest = (&timestart[0]);
			char timeend[3];
			int en = ran + (rand() % 3 + 1);
			timeend[0] = en / 10 + '0';
			timeend[1] = en % 10 + '0';
			timeend[2] = '\0';
			std::string timeen = (&timeend[0]);
			next["available_schedules"].append(weeks[(rand() % 2 + 1 + (7 - j)) % 7] + timest + ":00--" + timeen + ":00");
		}
		for (j = rand() % 5; j >= 0; j--) {
			k[5 - j] = rand() % a;
			for (e = 5 - j; e >= 0; e--) {
				if (k[5 - j] == k[e] && 5 - j != e) {
					e = 8;
					break;
				}
			}
			if (e == 8) {
				continue;
			}
			next["want_departments"].append(name[k[5 - j]]);
		}
		int gpa ;
		gpa = rand() %100;
		next["GPA"]=(gpa);
		gpa = 0;
		root["students"].append(next);
		next.clear();
	}
	std::ofstream ofs;
	std::string title1;
	std::string title2;
	char stude[6];
	char depar[4];
	depar[0] = a / 100 + '0';
	depar[1] = (a / 10) % 10 + '0';
	depar[2] = a % 10 + '0';
	depar[3] = '\0';
	title2 = depar;
	stude[0] = b / 10000 + '0';
	stude[1] = (b / 1000) % 10 + '0';
	stude[2] = (b / 100) % 10 + '0';
	stude[3] = (b / 10) % 10 + '0';
	stude[4] = (b % 10) + '0';
	stude[5] = '\0';
	title1 = stude;
	ofs.open("s" + title1 + "-d" + title2 + "-out.json");
	ofs << swriter.write(root);
	ofs.close();
	return 0;
}