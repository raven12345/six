#include"json/json.h"
#include<iostream>
#include<string.h>
#include<istream>
#include<fstream>
#include<assert.h>
using namespace std;
#pragma comment(lib, "lib_json.lib")
class departments {
public:
	int departments_no;					//社团编号
	std::string departments_name;		//社团名
	int member_limit;					//社团要收的人数
	int tag_num;						//社团标签数
	string* tags;						//社团标签动态数组		
	int event_num;					//社团活动时间数目
	int** event_schedules;	//社团活动时间动态数组(第〇列星期，第一列起始时间，第二列终止时间
	int num_member = 0;					//已经收了的成员数
	int *member;						//已收的成员(动态数组(初始10000
	departments() {};
	void setnum(Json::Value departs) {
		int i;
		int a = departs["tags"].size();
		tag_num = a;
		std::string departments_no_ = departs["department_no"].asString();
		departments_no = (departments_no_[4] - ('0' - 0)) * 100 + (departments_no_[5] - ('0' - 0)) * 10 + (departments_no_[6] - ('0' - 0));
		departments_name = departs["department_name"].asString();
		member_limit = departs["member_limit"].asInt();
		if (member_limit != 0) {
			member = new int[member_limit];
			for (i = 0; i < member_limit; i++)
			{
				member[i] = 10000;
			}
		}
		else {
			member = NULL;
		}
		tags = new std::string[tag_num];
		for (i = 0; i < a; i++) {
			tags[i] = departs["tags"][i].asString();
		}
		a = departs["event_schedules"].size();
		event_num = a;
		event_schedules = new int*[a];
		for (i = 0; i < a; i++) {
			event_schedules[i] = new int[3];
		}
		for (i = 0; i < a; i++) {
			std::string departments_time = departs["event_schedules"][i].asString();
			if (departments_time[0] == 'M') {
				event_schedules[i][0] = 1;
			}
			else if (departments_time[0] == 'T') {
				if (departments_time[1] == 'u') {
					event_schedules[i][0] = 2;
				}
				else {
					event_schedules[i][0] = 4;
				}
			}
			else if (departments_time[0] == 'W') {
				event_schedules[i][0] = 3;
			}
			else if (departments_time[0] == 'F') {
				event_schedules[i][0] = 5;
			}
			else if (departments_time[0] == 'S') {
				if (departments_time[1] == 'a') {
					event_schedules[i][0] = 6;
				}
				else if (departments_time[1] == 'u') {
					event_schedules[i][0] = 7;
				}
			}
			else {
				break;
			}
			event_schedules[i][1] = (departments_time[3] - ('0' - 0)) * 10 + (departments_time[4] - ('0' - 0));
			event_schedules[i][2] = (departments_time[10] - ('0' - 0)) * 10 + (departments_time[11] - ('0' - 0));
		}
	}
};
class student {
public:
	int student_no;						//学生学号
	std::string student_name;			//学生姓名
	int tag_num;						//学生标签数量
	std::string* tags;					//学生标签动态数组
	int available_num;					//学生活动时间数量
	int **available_schedules;			//学生活动时间动态数组
	int want_num;						//学生报了的部门数量
	int *want_department;				//学生想要报的部门
	int gpa;							//学生的绩点
	int num_in = 0;
	int *whatin;						//学生进入了的部门(初始200
	student() {};
	void setnum(Json::Value stu) {
		cout << 3;
		int i;
		int a = stu["tags"].size();
		tag_num = a;
		tags = new std::string[a];
		std::string students_no_ = stu["student_no"].asString();
		student_no = (students_no_[0] - ('0' - 0)) * 10000 + (students_no_[1] - ('0' - 0)) * 1000 + (students_no_[2] - ('0' - 0))*100+ (students_no_[3] - ('0' - 0))*10+ (students_no_[4] - ('0' - 0) );
		student_name = stu["student_name"].asString();
		want_num = stu["want_departments"].size();
		want_department = new int[want_num];
		whatin = new int[want_num];
		for (i = 0; i < want_num; i++) {
			whatin[i]=200;
		}
		cout << 4;
		for (i = 0; i < a; i++) {
			tags[i] = stu["tags"][i].asString();
		}
		a = stu["available_schedules"].size();
		available_num = a;
		available_schedules = new int*[a];
		for (i = 0; i < a; i++) {
			available_schedules[i] = new int[3];
		}
		cout << 5;
		for (i = 0; i < a; i++) {
			std::string students_time = stu["available_schedules"][i].asString();
			cout << 6;
			if (students_time[0] == 'M') {
				available_schedules[i][0] = 1;
			}
			else if (students_time[0] == 'T') {
				if (students_time[1] == 'u') {
					available_schedules[i][0] = 2;
				}
				else {
					available_schedules[i][0] = 4;
				}
			}
			else if (students_time[0] == 'W') {
				available_schedules[i][0] = 3;
			}
			else if (students_time[0] == 'F') {
				available_schedules[i][0] = 5;
			}
			else if (students_time[0] == 'S') {
				if (students_time[1] == 'a') {
					available_schedules[i][0] = 6;
				}
				else if (students_time[1] == 'u') {
					available_schedules[i][0] = 7;
				}
			}
			else {
				break;
			}
			cout << 7;
			available_schedules[i][1] = (students_time[3] - ('0' - 0)) * 10 + (students_time[4] - ('0' - 0));
			available_schedules[i][2] = (students_time[10] - ('0' - 0)) * 10 + (students_time[11] - ('0' - 0));
		}
		int h = stu["want_departments"].size();
		for (i =0; i <h; i++) {
			std::string department = stu["want_departments"][i].asString();
			want_department[i] = (department[4] - ('0' - 0)) * 100 + (department[5] - ('0' - 0)) * 10 + (department[6] - ('0' - 0));
		}
		gpa = stu["GPA"].asInt();
	}
};
class systems {
	
public:
	Json::Value project(Json::Value root) {


		departments* department;
		student* students;
		int i, j, k, l;		//循环用
		int a;			//社团数
		int b;			//学生数
		int c;			//某学生所选的社团数
		int d;			//时间冲突数
		int e;			//判别是否由于时间冲突不能进入该部
		int *f;

		a = root["departments"].size();
		department = new departments[a];
		for (i = 0; i < a; i++) {
			Json::Value depar = root["departments"][i];
			department[i].setnum(depar);
		}
		b = root["students"].size();
		f = new int[b];
		for (i = 0; i < b; i++) {
			f[i] = i;
		}
		students = new student[b];
		for (i = 0; i < b; i++) {
			students[i].setnum(root["students"][i]);
		}
		//数据存储完成
		//按学号顺序
		for (i = 0; i < b; i++) {
			d = 0;			//此处用来记录第几个为最大
			e = 0;			//此处用来记录最大值
			for (j = i; j < b; j++) {
				if (students[f[i]].gpa > e) {
					d = i;
				}
			}
			e = f[d];
			f[d] = f[i];
			f[i] = e;
		}
		for (i = 0; i < b; i++) {
			c = students[f[i]].want_num;
			for (j = 0; j < c; j++) {
				department[students[f[i]].want_department[j]];
				if (department[students[f[i]].want_department[j]].member_limit > department[students[f[i]].want_department[j]].num_member) {	//还有空位的情况下
					cout << 10;
					d = 0;
					e = 0;
					for (k = 0; k < department[students[f[i]].want_department[j]].event_num; k++) {
						for (l = 0; l < students[f[i]].available_num; l++) {
							if (students[f[i]].available_schedules[l][0] == department[students[f[i]].want_department[j]].event_schedules[k][0]) {
								if (students[f[i]].available_schedules[l][1] <= department[students[f[i]].want_department[j]].event_schedules[k][1]) {
									if (students[f[i]].available_schedules[l][2] >= department[students[f[i]].want_department[j]].event_schedules[k][2]) {
										break;
									}
								}
							}
						}
						if (l == students[f[i]].available_num) {
							d = d + 1;
							if (d >2) {
								e = 1;
								break;
							}
						}
					}
					if (e == 1) {
					}
					else {
						for (k = 0; k < department[students[f[i]].want_department[j]].event_num; k++) {
							for (l = 0; l < students[f[i]].available_num; l++) {
								if (students[f[i]].available_schedules[l][0] == department[students[f[i]].want_department[j]].event_schedules[k][0]) {
									if (students[f[i]].available_schedules[l][1] <= department[students[f[i]].want_department[j]].event_schedules[k][1]) {
										if (students[f[i]].available_schedules[l][2] >= department[students[f[i]].want_department[j]].event_schedules[k][2]) {
											students[f[i]].available_schedules[l][2] = students[f[i]].available_schedules[l][1];	//学生的一个活动时间只允许参加一个部门项目；
											break;
										}
									}
								}
							}
						}
						students[f[i]].whatin[students[f[i]].num_in] = students[f[i]].want_department[j];			//将部门记入学生
						department[students[f[i]].want_department[j]].member[department[students[f[i]].want_department[j]].num_member] = f[i];		//将学生记入部门
						department[students[f[i]].want_department[j]].num_member++;						//部门成员数++
						students[f[i]].num_in++;						//学生入部数++
					}
				}
			}
		}
		Json::Value t;
		Json::Value m;
		char no[6];
		root.clear();
		for (i = 0; i < b; i++) {
			if (students[i].num_in != 0) {
				t["student_name"] = students[i].student_name;
				no[0] = students[i].student_no / 10000 + '0';
				no[1] = (students[i].student_no / 1000) % 10 + '0';
				no[2] = (students[i].student_no / 100) % 10 + '0';
				no[3] = (students[i].student_no / 10) % 10 + '0';
				no[4] = students[i].student_no % 10 + '0';
				no[5] = '\0';
				t["student_no"] = no;
				for (j = 0; j < students[i].num_in; j++) {
					m["department_no"] = students[i].whatin[j];
					m["department_name"] = department[students[i].whatin[j]].departments_name;
					t["student_departments"].append(m);
					m.clear();
				}
				root["students_with_departments"].append(t);
				t.clear();
			}
			else {
				t["student_name"] = students[i].student_name;
				no[0] = students[i].student_no / 10000 + '0';
				no[1] = (students[i].student_no / 1000) % 10 + '0';
				no[2] = (students[i].student_no / 100) % 10 + '0';
				no[3] = (students[i].student_no / 10) % 10 + '0';
				no[4] = students[i].student_no % 10 + '0';
				no[5] = '\0';
				t["student_no"] = no;
				root["students_no_departments"].append(t);
				t.clear();
			}
		}
		char deno[8];
		deno[0] = 'F';
		deno[1] = 'Z';
		deno[2] = 'U';
		deno[3] = '_';
		deno[7] = '\0';
		for (i = 0; i < a; i++) {
			if (department[i].num_member != 0) {
				deno[4] = department[i].departments_no / 100 + '0';
				deno[5] = (department[i].departments_no / 10) % 10 + '0';
				deno[6] = department[i].departments_no % 10 + '0';
				t["department_no"] = deno;
				t["department_name"] = department[i].departments_name;
				for (j = 0; j < department[i].num_member; j++) {
					m["student_no"] = department[i].member[j];
					m["student_name"] = students[department[i].member[j]].student_name;
					t["department_students"].append(m);
					m.clear();
				}
				root["department_with_students"].append(t);
				t.clear();
			}
			else {
				deno[4] = department[i].departments_no / 100 + '0';
				deno[5] = (department[i].departments_no / 10) % 10 + '0';
				deno[6] = department[i].departments_no % 10 + '0';
				t["department_no"] = deno;
				t["department_name"] = department[i].departments_name;
				root["department_no_students"].append(t);
				t.clear();
			}
		}

		return root;
	}
	Json::Value read(std::string filename) {
		Json::Value root;
		ifstream fin;
		Json::Reader reader;
		fin.open(filename);
		assert(fin.is_open());
		if (!reader.parse(fin, root, false)) {
			return -1;
		}
		fin.close();
		return root;
	}
	void put(Json::Value root) {
		int a;
		int b;
		ofstream ofs;
		a = root["departments"].size();
		b = root["students"].size();
		std::string title1;
		std::string title2;
		char stude[6];
		char depar[4];
		depar[0] = a / 100 + '0';
		depar[1] = (a / 10) % 10 + '0';
		depar[2] = a % 10 + '0';
		depar[3] = '\0';
		title2 = depar;
		stude[0] = b / 10000 + '0';
		stude[1] = (b / 1000) % 10 + '0';
		stude[2] = (b / 100) % 10 + '0';
		stude[3] = (b / 10) % 10 + '0';
		stude[4] = (b % 10) + '0';
		stude[5] = '\0';
		title1 = stude;
		ofs.open("s" + title1 + "-d" + title2 + "-out.json");
		ofs << root;
		ofs.close();
	}
};
int main() {
	int a;
	int b;
	systems sys;
	Json::Value root;
	std::string filename;			//存储文件名
	cin >> filename;
	root = sys.read(filename);
	root = sys.project(root);
	sys.put(root);
	return 0;
}